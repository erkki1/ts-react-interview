import React, { useCallback, useState } from "react";
import { Todo, TodoItem } from "./Todo";

const initialTodos: TodoItem[] = [
  {
    id: "a",
    content: "first one",
  },
  {
    id: "b",
    content: "second one",
  },
];

export const TodoApp = () => {
  const [todos, setToDos] = useState<TodoItem[]>(initialTodos);

  const onSave = useCallback(
    (item: TodoItem) => {
      console.log(item);

      const index = todos.findIndex((x) => x.id === item.id);

      const newTodos = [...todos];

      newTodos.splice(index, 1, item);

      setToDos(newTodos);
    },
    [todos]
  );

  const onDelete = useCallback((item: TodoItem) => {
    setToDos((current) => current.filter((x) => x.id !== item.id));
  }, []);

  const onAddTodo = useCallback(() => {
    setToDos((prev) => [
      ...prev,
      { id: Date.now().toString(), content: "placeholder" },
    ]);
  }, []);

  return (
    <div>
      <button onClick={onAddTodo}>Add</button>
      {todos.map((todo) => (
        <Todo key={todo.id} item={todo} onSave={onSave} onDelete={onDelete} />
      ))}
    </div>
  );
};
