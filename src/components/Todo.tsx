import React, { ChangeEvent, useCallback, useState } from "react";

export type TodoItem = {
  id: string;
  content: string;
};

type Props = {
  item: TodoItem;
  onSave: (item: TodoItem) => void;
  onDelete: (item: TodoItem) => void;
};

export const Todo = ({ item, onSave, onDelete }: Props) => {
  const [isEditing, setIsEditing] = useState(false);

  const [contentInternal, setContentInternal] = useState(item.content);

  const handleEdit = useCallback(() => {
    setIsEditing(true);
  }, []);

  const handleDelete = useCallback(() => {
    onDelete(item);
  }, [item, onDelete]);

  const handleSave = useCallback(() => {
    setIsEditing(false);
    onSave({ id: item.id, content: contentInternal });
  }, [contentInternal, item.id, onSave]);

  const handleCancel = useCallback(() => {
    setIsEditing(false);
  }, []);

  const onContentChange = (e: ChangeEvent<HTMLInputElement>) => {
    setContentInternal(e.target.value);
  };

  return (
    <div>
      <span>
        {isEditing ? (
          <>
            <button onClick={handleSave}>Save</button>
            <button onClick={handleCancel}>Cancel</button>
            <input
              type="text"
              value={contentInternal}
              onChange={onContentChange}
            />
          </>
        ) : (
          <>
            <button onClick={handleEdit}>Edit</button>
            <button onClick={handleDelete}>Delete</button>
            <span>{item.content}</span>
          </>
        )}
      </span>
    </div>
  );
};
